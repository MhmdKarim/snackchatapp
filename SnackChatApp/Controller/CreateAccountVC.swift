//
//  CreateAccountVC.swift
//  SnackChatApp
//
//  Created by Karim on 4/13/18.
//  Copyright © 2018 Karim. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func closeBtnPressed(_ sender: Any) {
        performSegue(withIdentifier: UNWIND, sender: nil)
    }
}

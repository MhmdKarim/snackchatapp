//
//  LoginVC.swift
//  SnackChatApp
//
//  Created by Karim on 4/13/18.
//  Copyright © 2018 Karim. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

        override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func closeBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createAccountBtnPressed(_ sender: Any) {
        performSegue(withIdentifier: TO_CREATE_ACCOUNT, sender: nil)
    }
    
}
